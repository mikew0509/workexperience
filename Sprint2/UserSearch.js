window.addEventListener("DOMContentLoaded", function(){

  var search = document.getElementById("search-input");
  var contentItem = document.getElementById("contentItem");
  
  search.addEventListener('change', function(){
   
    let searchTerm = search.value.replace(' ', '+');
  fetch(`https://api.tfl.gov.uk/Line/${searchTerm}/Status?detail=false`)
    .then(res => res.json())
    .then(data => {
      contentItem.innerHTML = data[0].lineStatuses[0].statusSeverityDescription
    });
  
  })});   
  
  