var contentItems = document.getElementsByClassName("contentItem")

fetch(`https://api.tfl.gov.uk/Line/Mode/tube/Status`)
  .then(res => res.json())
  .then(data => {
    for (i = 0; i < data.length; i++) {
      for (j = 0; j < contentItems.length; j++) {
        if (contentItems[j].innerHTML.includes(data[i].name)) {
          contentItems[j].innerHTML = data[i].name + " Line : " + data[i].lineStatuses[0].statusSeverityDescription
        }
        if (contentItems[j].innerHTML.includes("Good")) {
          contentItems[j].setAttribute("style", "color:green")
        }
        else {
          contentItems[j].setAttribute("style", "color:red")
        }
      }
    }
});
