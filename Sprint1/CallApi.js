var contentItems = document.getElementsByClassName("contentItem")

fetch(`https://api.tfl.gov.uk/Line/jubilee/Status?detail=false`)
  .then(res => res.json())
  .then(data => {
    for (i = 0; i < contentItems.length; i++) {
      if (contentItems[i].innerHTML.includes("Jubilee")) {
        contentItems[i].innerHTML = "Jubilee Line : " + data[0].lineStatuses[0].statusSeverityDescription
      }
      if (contentItems[i].innerHTML.includes("Good")) {
		    contentItems[i].setAttribute("style", "color:green")
      }
      else {
        contentItems[i].setAttribute("style", "color:red")
      }
    }
});
